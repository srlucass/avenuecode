package pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyTasksPage extends PageBase {


	public MyTasksPage(WebDriver driver) {
		super(driver);
	}

	public void clickMyTasks(){
		getDriver().findElement(By.linkText("My Tasks")).click();
	}
	
	public void enterTaskDescription(String chars){
		getDriver().findElement(By.id("new_task")).clear();
		getDriver().findElement(By.id("new_task")).sendKeys(chars);
	}
	
	public void enterSubTaskDescription(String chars){
		getDriver().findElement(By.id("new_sub_task")).clear();
		getDriver().findElement(By.id("new_sub_task")).sendKeys(chars);
	}
	
	public void enterDueDate(String date){
		getDriver().findElement(By.id("dueDate")).clear();
		getDriver().findElement(By.id("dueDate")).sendKeys(date);
	}
	
	public void hitEnterTask(){
		getDriver().findElement(By.id("new_task")).click();
		getDriver().findElement(By.id("new_task")).submit();
	}
	
	public void clickAddBtn(){
		getDriver().findElement(By.cssSelector("span.input-group-addon.glyphicon.glyphicon-plus")).click();
	}
	
	public void clickAddSubTaskBtn(){
		getDriver().findElement(By.id("add-subtask")).click();
	}
	
	public boolean removeTask(String task){
		return actionTableElementByTaskDescription(task, "REMOVE","");
	}
	
	public boolean removeSubTask(String task){
		return actionTableElementByTaskDescription(task, "REMOVE","SUB");
	}
	
	public void clickManageSubTasks(String task){
		actionTableElementByTaskDescription(task, "SUBTASK","");
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfAllElements(getDriver().findElements(By.xpath("//div[@class='modal-dialog']"))));
	}
	
	public void clickDone_Task(String task){
		actionTableElementByTaskDescription(task, "DONE", "");
	}
	
	public void clickDone_SubTask(String task){
		actionTableElementByTaskDescription(task, "DONE", "SUB");
	}
	
	public void clickPublic(String task){
		actionTableElementByTaskDescription(task, "PUBLIC","");
	}
	
	public boolean isPresentTaskElementTable(String task){
		return actionTableElementByTaskDescription(task, "CHECK_TASK", "");
	}
	
	public boolean isPresentSubTaskElementTable(String task){
		return actionTableElementByTaskDescription(task, "CHECK_TASK", "SUB");
	}
	
	public boolean isPresentModalDialogSubTask(){
		return getDriver().findElement(By.xpath("//div[@class='modal-dialog']")).isDisplayed();
	}
	
	private boolean actionTableElementByTaskDescription(String parameter, String action, String identifier){
		WebElement table = null;
		if(identifier.equals("SUB")){
			table = getDriver().findElement(By.xpath("//div[@class='modal-dialog']//table[@class='table']/tbody"));
		}else{
			table = getDriver().findElement(By.xpath("//table[@class='table']/tbody"));
		}
		
        List<WebElement> trs = table.findElements(By.xpath("tr"));
        
        for(WebElement tr : trs)
        {
            List<WebElement> tds = tr.findElements(By.xpath("td"));
            boolean isValidElement = false;
            for (int i = 0; i < tds.size(); i++) {
            	
				if(tds.get(i).getText().equalsIgnoreCase(parameter) && action.equals("CHECK_TASK")){
					return true;
				}
				
				if(tds.get(i).getText().equalsIgnoreCase(parameter)){
					isValidElement = true;
				}
			
				if (isValidElement && action.equals("REMOVE") && tds.get(i).getText().contains("Remove")){
					tds.get(i).findElement(By.xpath("//button[@class='btn btn-xs btn-danger']")).click();
            		return true;
				}else if (isValidElement && action.equals("SUBTASK") && tds.get(i).getText().contains("Manage")){
					tds.get(i).findElement(By.xpath("//button[@class='btn btn-xs btn-primary ng-binding']")).click();
            		return true;
				}else if (isValidElement && action.equals("DONE")){
					tds.get(0).findElement(By.xpath("//input[@class='ng-pristine ng-untouched ng-valid']")).click();
            		return true;
            	}else if (isValidElement && action.equals("PUBLIC")){
					tds.get(3).findElement(By.xpath("//input[@class='ng-pristine ng-untouched ng-valid']")).click();
            		return true;
            	}
			}
        }
        return false;        
	}
	
	public int getRowNum(String identifier){
		WebElement table = null;
		if(identifier.equals("SUB")){
			table = getDriver().findElement(By.xpath("//div[@class='modal-dialog']//table[@class='table']/tbody"));
		}else{
			table = getDriver().findElement(By.xpath("//table[@class='table']/tbody"));
		}
        List<WebElement> trs = table.findElements(By.xpath("tr"));
        
        return trs.size();
	}

	public boolean isValidTaskId(){
		String taskId =  getDriver().findElement(By.xpath("//h3")).getText();
		if(taskId.contains("Editing Task")){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isValidTaskDescription_Sub(){
		String readonly = getDriver().findElement(By.id("edit_task")).getAttribute("readonly");
		if (readonly != null){
			return true;
		}else{
			return false;
		}
	}
	
	public String getMessage(){
		return getDriver().findElement(By.xpath("//h1")).getText();
	}
	
	public String getUserLogged(){
		return getDriver().findElement(By.xpath("//a[contains(text(),'Welcome')]")).getText();
	}
	
	public String getManageSubTaskLabel(String task){
		WebElement table =getDriver().findElement(By.xpath("//table[@class='table']/tbody"));
        List<WebElement> trs = table.findElements(By.xpath("tr"));
        for(WebElement tr : trs)
        {
            List<WebElement> tds = tr.findElements(By.xpath("td"));
            boolean isValidElement = false;
            for (WebElement webElement : tds) {
            	if(webElement.getText().equalsIgnoreCase(task)){
					isValidElement = true;
				}
            	if(isValidElement && webElement.getText().contains("Manage")){
            		return webElement.findElement(By.xpath("//button[@class='btn btn-xs btn-primary ng-binding']")).getText();
            	}
			}
        }
        return null;
	}
}
