package pageobjects;

import org.openqa.selenium.WebDriver;

public class PageBase {

	private WebDriver driver;

	public PageBase(WebDriver driver) {
		this.driver=driver;
	}

	public void navigateTo(String url) {
		driver.navigate().to(url);
	}

	public WebDriver getDriver() {
		return driver;
	}

	

}
