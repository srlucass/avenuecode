package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/report"},features = "Feature",glue={"steps"},tags = {"@Task,@Subtask"})

public class Runner 
{
	
}
