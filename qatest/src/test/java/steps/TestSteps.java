package steps;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.MyTasksPage;

public class TestSteps {
	
	private WebDriver driver;
	private DesiredCapabilities capabilities;
	private String taskName;
	private int rownum;
	
	private MyTasksPage tasksPage;
	
	@Before
	public void init(){
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
	    options.addArguments("--start-maximized");

	    capabilities = DesiredCapabilities.chrome();
	    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	}

	@Given("^User is on Home Page$")
	public void accessHome(){
		driver = new ChromeDriver(capabilities);
		tasksPage = new MyTasksPage(driver);
		login();
	}
	
	@Given("^User access Subtask menu$")
	public void accessSubtask(){
		tasksPage.clickManageSubTasks(taskName);
	}

	@When("^User Navigate to MyTask Page$")
	public void accessMyTaskPage(){
		tasksPage.clickMyTasks();
	}
	
	@And("^Enter description \"([^\"]*)\"$")
	public void enterDescription(String description){
		tasksPage.enterTaskDescription(description);
		taskName = description;
	}
	
	@And("^Enter subtask description \"([^\"]*)\"$")
	public void enterSubTaskDescription(String description){
		tasksPage.enterSubTaskDescription(description);
		taskName = description;
	}
	
	@And("^Access subtask of \"([^\"]*)\"$")
	public void enterSubtask(String description){
		tasksPage.clickManageSubTasks(description);
		taskName = description;
	}
	
	@And("^Enter Due Date \"([^\"]*)\"$")
	public void enterDueDate(String date){
		tasksPage.enterDueDate(date);
	}
	
	@And("^Click in Add button$")
	public void clickAddbtn(){
		tasksPage.clickAddBtn();
	}
	
	@And("^Click in Add SubTask button$")
	public void clickAddSubbtn(){
		tasksPage.clickAddSubTaskBtn();
	}
	
	@And("^Hit Enter key$")
	public void hitEnter(){
		tasksPage.hitEnterTask();
	}
	
	@And("^List with subtasks is displayed$")
	public void getRowNum(){
		rownum = tasksPage.getRowNum("SUB");
	}
	
	@And("^User can close modal to verify numbers of subtaks in the button$")
	public void checkManageSubTaskBtn(){
		Assert.assertEquals("("+rownum+") Manage Subtasks", tasksPage.getManageSubTaskLabel(taskName));
	}
	
	@And("^Task ID is available and read-only$")
	public void checktaskId(){
		Assert.assertEquals(true, tasksPage.isValidTaskId());
	}
	
	@Then("^Message displayed Successfully$")
	public void checkMyTaskMessage(){
		String user = tasksPage.getUserLogged().split(" ")[1];
		Assert.assertEquals("Hey " + user + ", this is your todo list for today:", tasksPage.getMessage());
	}
	
	@Then("^Task is appended on the list$")
	public void checkTask(){
		Assert.assertTrue("Task was not appended on the list", tasksPage.isPresentTaskElementTable(taskName));
	}
	
	@Then("^Task is not appended on the list$")
	public void checkTaskNegative(){
		Assert.assertFalse("Task was appended on the list", tasksPage.isPresentTaskElementTable(taskName));
	}
	
	@Then("^Modal dialog displayed Successfully$")
	public void checkDialog(){
		Assert.assertTrue("Sub Task Modal is not being displayed", tasksPage.isPresentModalDialogSubTask());
	}
	
	@Then("^SubTask is not appended on the list$")
	public void checkSubTaskNeg(){
		Assert.assertFalse("SubTask was appended on the list", tasksPage.isPresentSubTaskElementTable(taskName));
	}
	
	@Then("^SubTask is appended on the list$")
	public void checkSubTask(){
		Assert.assertTrue("SubTask was not appended on the list", tasksPage.isPresentSubTaskElementTable(taskName));
	}
	
	@Then("^Empty SubTask is not appended on the list$")
	public void checkSubTaskEmpty(){
		Assert.assertFalse("SubTask without description and due date was appended on the list", tasksPage.isPresentSubTaskElementTable("empty"));
	}
	
	@Then("^Task description is available and read-only$")
	public void checkTaskDescription(){
		Assert.assertTrue("Task Description is not read-only",tasksPage.isValidTaskDescription_Sub());
	}
	
	private void login() {
		driver.navigate().to("http://qa-test.avenuecode.com/");
		driver.findElement(By.linkText("Sign In")).click();
		driver.findElement(By.id("user_email")).sendKeys("lucas.souzaromao@gmail.com");
		driver.findElement(By.id("user_password")).sendKeys("123456789");
		driver.findElement(By.xpath(".//input[@class='btn btn-primary']")).click();
	}
	
	@After
	public void clean(){
		driver.close();
	}
	
}
