@Task
Feature: Create Task
As a ToDo App user
I should be able to create a task
So I can manage my tasks

Scenario: Successful Access My Task Page
	Given User is on Home Page
	When User Navigate to MyTask Page
	Then Message displayed Successfully

	
Scenario: User Can Enter a New Task with Add Button
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Enter description "Automation QA Test BTN"
	And Click in Add button
	Then Task is appended on the list
	
Scenario: User Can Enter a New Task Hitting Enter
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Enter description "Automation QA Test Enter"
	And Hit Enter key
	Then Task is appended on the list
	
Scenario: User Cannot Create a Task with less than 3 characters 
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Enter description "Au"
	And Click in Add button
	Then Task is not appended on the list
	
Scenario: User Cannot Create a Task with more than 250 characters 
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Enter description "*@#$%¨&*()+==//<>:?/||\\'__-- Test Task Description with more than 250 characters *@#$%¨&*()+==//<>:?/||\\'__--, 2º *@#$%¨&*()+==//<>:?/||\\'__-- Test Task Description with more than 250 characters *@#$%¨&*()+==//<>:?/||\\'__-- Final characters 123456"
	And Click in Add button
	Then Task is not appended on the list
	