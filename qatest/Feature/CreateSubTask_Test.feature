@Subtask
Feature: Create SubTask
As a ToDo App user
I should be able to create a subtask
So I can break down my tasks in smaller pieces


Scenario: User Can Enter a New SubTask with Add Button
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Access subtask of "Automation QA Test BTN"
	And Enter subtask description "Automation QA SubtTask BTN"
	And Click in Add SubTask button
	Then SubTask is appended on the list

Scenario: User cannot edit Task ID and Task description
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Access subtask of "Automation QA Test BTN"
	Then Task description is available and read-only
	And Task ID is available and read-only
	
Scenario: User Cannot Create a Task with more than 250 characters 
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Access subtask of "Automation QA Test BTN"
	And Enter subtask description "*@#$%¨&*()+==//<>:?/||\\'__-- Test Task Description with more than 250 characters *@#$%¨&*()+==//<>:?/||\\'__--, 2º *@#$%¨&*()+==//<>:?/||\\'__-- Test Task Description with more than 250 characters *@#$%¨&*()+==//<>:?/||\\'__-- Final characters 123456"
	And Click in Add SubTask button
	Then SubTask is not appended on the list
	
Scenario: User Cannot Create a SubTask without SubTask Description and Due Date
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Access subtask of "Automation QA Test BTN"
	And Click in Add SubTask button
	Then Empty SubTask is not appended on the list
	
Scenario: User Cannot Create a SubTask with Due Date different of MM/dd/yyyy
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Access subtask of "Automation QA Test BTN"
	And Enter subtask description "Due Date with wrong format"
	And Enter Due Date "1998/01/27"
	And Click in Add SubTask button
	Then SubTask is not appended on the list
	
Scenario: User can see a label Manage Subtasks
	Given User is on Home Page
	When User Navigate to MyTask Page
	And Access subtask of "Automation QA Test BTN"
	Then Modal dialog displayed Successfully
	And List with subtasks is displayed
	And User can close modal to verify numbers of subtaks in the button 
